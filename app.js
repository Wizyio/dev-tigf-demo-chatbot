'use strict';
const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const app = express();
const connection = connect();

const config = require('./config');

// view engine setup
// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/chatroombot', require('./routes/chatroombot'));
app.use('/observerbot', require('./routes/observerbot'));

app.get('/', function(req, res) {
    console.log("servicechatbox opened");
    res.send(
        `<!DOCTYPE html>
        <html>
        <head>
            <title></title>
        </head>
        <body>
            <script>(function(){var e=document.createElement("script"),t=document.createElement("a"),c=(new Date).getTime(),n="//localhost:3001/static/loader.js?t=${config.serviceRoomToken}&name=service test 001";t.href=n;var a=t.search?"&":"?";e.src=n+a+"v="+c,e.async=!0,document.body.appendChild(e)})()</script>
        </body>
        </html>`
    );
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});


const port = 4000;
connection
    .on('error', console.log)
    .on('disconnected', connect)
    .once('open', listen);

function listen () {
    app.listen(4000);
    console.log('Express app started on port ' + port);
}

function connect () {
    var options = { server: { socketOptions: { keepAlive: 1 } } };
    var connection = mongoose.connect('mongodb://admin:admin@ds111895.mlab.com:11895/tigf-chatbots', options).connection;
    return connection;
}

module.exports = app;
