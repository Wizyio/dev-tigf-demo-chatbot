// grab the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var userSchema = new Schema({
    firstName: {type: String, default: ''},
    lastName: {type: String, default: ''},
    gender: {type: String, default: ''},
    locale: {type: String, default: ''},
    timezone: {type: String, default: ''},
    email: {type: String, default: ''},
    oneToOneBotId: {type: String, default: ''},
    recipientId: {type: String, default: ''},
    workroomId: {type: String, default: ''},
    domainId: {type: String, default: ''},
    status: {type: String, default: ''},
    lastMessage: {type: String, default: ''},
    wizyRoomServiceUserKey: {type: String, default: ''},
    createdAt: Date,
    updatedAt: Date
});

// the schema is useless so far
// we need to create a model using it
var User = mongoose.model('User', userSchema);

// make this available to our users in our Node applications
module.exports = User;
