'use strict';
var request = require('request');

const config = require('../config');

const publicDevServer = config.publicServer;
const apiMessages = '/api/v1/messages';
const apiServiceMessages = '/api/v1/service/messages';
const apiServiceUsers = '/api/v1/service/users';
const apiMessagesURL = publicDevServer + apiMessages;
const apiServiceMessagesURL = publicDevServer + apiServiceMessages;
const apiServiceUsersURL = publicDevServer + apiServiceUsers;
const serviceRoomToken = config.serviceRoomToken;


module.exports = {
    sendMessage: (postData, callback) => {
        console.log(postData);
        request.post(apiMessagesURL, {
            json: postData,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }, (error, response, body) => {
            console.log('sendServiceMessage', body, error);
            callback(error, body);
        });
    },
    sendServiceMessage: (message, callback) => {
        console.log(message);
        request.post(apiServiceMessagesURL, {
            json: message,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'bearer ' + serviceRoomToken
            }
        }, (error, response, body) => {
            console.log('sendServiceMessage', body, error);
            callback(error, body);
        });
    },
    createServiceUser: (user, callback) => {
        var postData = {
            'name': user.name,
            'image_url': user.image_link,
        };
        console.log(postData, apiServiceUsersURL);
        request.post(apiServiceUsersURL, {
            json: postData,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'bearer ' + serviceRoomToken
            }
        }, (error, response, body) => {
            console.log('create user resp', body);
            callback(error, body.data.key);
        });
    }
};
