'use strict';
const apiai = require('apiai');
const request = require('request');

const config = require('../config');

const ApiAI = apiai(config.apiaiToken);


module.exports = {
    textRequest: (payload, successCallback, errorCallback) => {
        console.log("getIntent payload", payload);
        let apiRequest = ApiAI.textRequest(payload.text, {
            sessionId: payload.sessionId
        });

        apiRequest.on('response', (response) => {
            successCallback(response);
        });

        apiRequest.on('error', (error) => {
            errorCallback(error);
        });

        apiRequest.end();
    },
};
