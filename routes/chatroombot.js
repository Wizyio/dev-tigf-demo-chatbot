'use strict';
const express = require('express');
const mongoose = require('mongoose');

const User = require('./../models/users');
const wizyroom = require('./../lib/wizyroom');

const router = express.Router();


router.post('/', function(req, res) {
    console.log('req body', req.body)
    const _user = req.body;
    const _userId = _user.message.owner_id
    User.findOne({'oneToOneBotId': _userId }, 'wizyRoomServiceUserKey', function(err, user) {
        console.log('one_to_one_bot user', user)
        let svcUserMessage = {
            body: _user.message.body,
        };

        if (user) {
            svcUserMessage.service_user = {key: user.wizyRoomServiceUserKey};
            wizyroom.sendServiceMessage(svcUserMessage, function(err, resp) {
                res.sendStatus(200);
            });
        } else {
            const imageUrl = 'https://vignette.wikia.nocookie.net/doraemon/images/4/41/Joy.png/revision/latest?cb=20120702110314&path-prefix=en';
            wizyroom.createServiceUser({
                name: _user.message.owner_name,
                image_link: imageUrl,
            }, function(err, serviceUserKey) {
                console.log('serviceUserKey', serviceUserKey)
                let newUser = new User({
                    oneToOneBotId: _userId,
                    recipientId: _user.bot.id,
                    workroomId: _user.message.workroom_id,
                    domainId: _user.message.domain_id,
                    wizyRoomServiceUserKey: serviceUserKey,
                    firstName: _user.message.owner_name,
                    lastName: _user.message.owner_name,
                    profilePic: imageUrl,
                });
                svcUserMessage.service_user =  {
                    key: serviceUserKey
                };
                console.log('newUser', newUser.wizyRoomServiceUserKey);
                newUser.save(function(er, u) {
                    console.log('sending to wizyroom', err, u)
                    wizyroom.sendServiceMessage(svcUserMessage, function(err, message) {
                        res.sendStatus(200)
                    });
                });
            });
        }
    })
});

router.get('/', function(req, res) {
});

module.exports = router;


