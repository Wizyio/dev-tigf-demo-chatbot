'use strict';
const express = require('express');
const mongoose = require('mongoose');

const User = require('./../models/users');
const config = require('../config');
const wizyroom = require('./../lib/wizyroom');
const apiai = require('./../lib/apiai');

const router = express.Router();


router.post('/', function(req, res) {
    console.log('observerbot receives message', req.body);
    const message = req.body.message;

    User.findOne({'wizyRoomServiceUserKey': req.body.service_user.key}, 'domainId workroomId', function(err, user) {
        console.log('user', user);
        if (message.service_user === null) {
            // from agent
            console.log("observerbot forwarding message to serviceuser chatroom", req.body)
            wizyroom.sendMessage({
                domain_id: user.domainId,
                membership_id: config.userChatbotId,
                token: config.userChatbotToken, //should be Chatbot's token
                workroom_id: user.workroomId,
                body: message.body,
                parent: message.parent,
                service_user: message.service_user,
                do_broadcast: true
            }, () => {});

            res.sendStatus(200);
            return;
        } else {
            // from service user
            console.log('observerbot receives message from service user');
            const liveChatEnabled = !req.body.bot.forwarding;
            console.log(req.body.bot.forwarding, liveChatEnabled)
            if (liveChatEnabled) {
                res.sendStatus(200);
                return;
            } else {
                console.log('observerbot decides its reply message');
                let chatbotsMessage = 'this is a intelligent message';
                apiai.textRequest({
                        'text': message.body,
                        'sessionId': message.service_user.key.substring(0, 15)
                    }, (response) => {
                        console.log("apiai response", response);
                        switch (response.result.action) {
                            case 'input.welcome':
                                chatbotsMessage = response.result.fulfillment.speech;
                                break;
                            case 'input.unknown':
                                chatbotsMessage = response.result.fulfillment.speech;
                            default:
                                chatbotsMessage = 'Sorry. I cannot undestand you right now.';
                                break;
                        };

                        console.log("observerbot sending his message in the serviceuser's chatroom");
                        wizyroom.sendMessage({
                            domain_id: user.domainId,
                            membership_id: config.userChatbotId,
                            token: config.userChatbotToken,
                            workroom_id: user.workroomId,
                            body: chatbotsMessage,
                            parent: message.parent,
                            service_user: message.service_user,
                            do_broadcast: true
                        }, () => {})

                        console.log('observerbot answering in behalf of the agent in the service room');
                        res.send({
                            body: chatbotsMessage,
                            is_reply: true,
                        });
                    }, (err) => {
                        console.log("errrr", err)
                        res.sendStatus(400);
                    }
                );
            }
        }
    });
});

router.get('/', function(req, res) {
});

module.exports = router;
